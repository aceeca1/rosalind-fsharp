#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let printfnSeq fmt s =
    let mutable head = true
    for i in s do
        if not head then printf " "
        head <- false
        printf fmt i
    printfn ""
let fasta1 (read: unit -> string) =
    read()
    let sb = System.Text.StringBuilder()
    let mutable s = read()
    while not (isNull s) do
        sb.Append(s)
        s <- read()
    sb.ToString()
let uniprot id =
    let url = sprintf "http://www.uniprot.org/uniprot/%s.fasta" id
    use res = System.Net.WebRequest.Create(url).GetResponse()
    use stream = res.GetResponseStream()
    use reader = new System.IO.StreamReader(stream)
    fasta1 reader.ReadLine
let re = System.Text.RegularExpressions.Regex("(?=N[^P][ST][^P])")
let ids = seq {
    let mutable finish = false
    while not finish do
        match read() with
        | null -> finish <- true
        | s -> yield s
}
for id in ids do
    let matches = re.Matches(uniprot id)
    let matchesT = matches.Cast<System.Text.RegularExpressions.Match>()
    let indexes = matchesT.Select(fun i -> i.Index + 1)
    if indexes.Any() then
        printfn "%s" id
        printfnSeq "%d" indexes
