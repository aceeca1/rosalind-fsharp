#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let square n = n * n
let ans = read().Split().Sum(int >> square)
printfn "%d" ans
