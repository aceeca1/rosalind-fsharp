#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let fasta = seq {
    let mutable name = ">"
    let mutable dna = System.Text.StringBuilder();
    while name <> "" do
        match read() with
        | null -> yield (name, dna.ToString()); name <- ""
        | s when s.[0] = '>' ->
            if name <> ">" then yield (name, dna.ToString())
            name <- s.[1 ..]
            dna.Clear()
        | s -> dna.Append(s)
}
let gc (dna:string) =
    double(dna.Count(fun i -> i = 'G' || i = 'C')) / double(dna.Length);;
let gc, name = fasta.Select(fun (name, dna) -> (gc dna, name)).Max()
printfn "%s" name
printfn "%.6f" (gc * 100.0)
