#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let [|a; b|] = read().Split() |> Array.map int
let ans = seq{a..b} |> Seq.sumBy(fun n -> if n &&& 1 = 0 then 0 else n)
printfn "%d" ans
