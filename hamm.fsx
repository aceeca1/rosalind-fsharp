#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
(Seq.map2 (fun i1 i2 -> i1 <> i2) (read()) (read())).Count(fun i -> i)
|> printfn "%d"
