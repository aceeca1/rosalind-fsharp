#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let fasta = seq {
    let mutable name = ">"
    let mutable dna = System.Text.StringBuilder();
    while name <> "" do
        match read() with
        | null -> yield (name, dna.ToString()); name <- ""
        | s when s.[0] = '>' ->
            if name <> ">" then yield (name, dna.ToString())
            name <- s.[1 ..]
            dna.Clear()
        | s -> dna.Append(s)
}
let rec bSearch f s t =
    if s = t then s else
        let m = (s + t) >>> 1
        if f(m) then bSearch f s m else bSearch f (m + 1) t
let dna = fasta.Select(snd).ToArray()
let slide k (d: string) =
    if k > d.Length then Seq.empty else seq {
        let dd = 0xdeaddead
        let mutable ddk = 1
        let mutable h = 0
        for i = 0 to k - 1 do
            h <- h * dd + int d.[i]
            ddk <- ddk * dd
        for i = k to d.Length - 1 do
            yield h
            h <- h * dd + int d.[i] - ddk * int d.[i - k]
        yield h
    }
let cs k = dna.Select(slide k).Aggregate(fun i1 i2 -> i1.Intersect(i2))
let m = dna.Select(fun i -> i.Length).Min()
let lcs = (bSearch (fun i -> Seq.isEmpty(cs i)) 1 (m + 1)) - 1
let h = (cs lcs).First()
let n = slide lcs dna.[0] |> Seq.findIndex(fun i -> i = h)
dna.[0].[n .. (n + lcs - 1)] |> printfn "%s"
