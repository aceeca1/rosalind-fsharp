#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let [|n; m|] = read().Split() |> Array.map int
seq {
    let a = Array.zeroCreate<bigint>(m + 1)
    a.[0] <- 1I
    a.[1] <- 1I
    a.[2] <- 1I
    let mutable i1, i2 = 2, 1
    while true do
        let i = (i1 + 1) % (m + 1)
        a.[i] <- a.[i1] + a.[i2] - a.[i]
        yield a.[i]
        i2 <- i1
        i1 <- i
} |> Seq.item (n - 3) |> printfn "%A"
