#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let s = read()
let [|a; b; c; d|] = read().Split() |> Array.map int
printfn "%s %s" s.[a..b] s.[c..d]
