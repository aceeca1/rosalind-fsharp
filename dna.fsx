#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let printfnSeq fmt s =
    let mutable head = true
    for i in s do
        if not head then printf " "
        head <- false
        printf fmt i
    printfn ""
let a = read() |> Seq.countBy id |> dict
"ACGT".Select(fun i -> snd(a.TryGetValue(i))) |> printfnSeq "%d"
