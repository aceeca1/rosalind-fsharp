#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let [|n; k|] = read().Split() |> Array.map int
seq {
    let mutable a1, a2 = 1, 0
    while true do
        yield a2
        let a0 = a1 + a2 * k
        a2 <- a1
        a1 <- a0
} |> Seq.item n |> printfn "%d"
