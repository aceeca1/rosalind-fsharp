#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let n = read().Split() |> Array.map double
let a = [|2.0; 2.0; 2.0; 1.5; 1.0; 0.0|]
Seq.map2 ( *) a n |> Seq.sum |> printfn "%.1f"
