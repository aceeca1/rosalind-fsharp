#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let rec f() =
    match read() with
    | null -> ()
    | _ -> read() |> printfn "%s"; f() in f()
