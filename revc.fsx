#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
new string(read().ToCharArray().Reverse().Select(fun i ->
    match i with
    | 'A' -> 'T'
    | 'C' -> 'G'
    | 'G' -> 'C'
    | 'T' -> 'A').ToArray()) |> printfn "%s"
