#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let [|k; m; n|] = read().Split() |> Array.map double
let population = k + m + n
let allCases = population * (population - 1.0)
let recessiveCases = 0.25 * m * (m - 1.0) + m * n + n * (n - 1.0)
let dominantCases = allCases - recessiveCases
dominantCases / allCases |> printfn "%.5f"
