#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let printfnSeq fmt s =
    let mutable head = true
    for i in s do
        if not head then printf " "
        head <- false
        printf fmt i
    printfn ""
let n = read() |> int
let a = [|1 .. n|]
let rec ppa = function
| i when i >= n -> printfnSeq "%d" a
| i ->
    let ai = a.[i]
    for j = n - 1 downto i do
        a.[i] <- a.[j]
        a.[j] <- ai
        ppa(i + 1)
        a.[j] <- a.[i]
ppa 0
