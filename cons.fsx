#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let fasta = seq {
    let mutable name = ">"
    let mutable dna = System.Text.StringBuilder();
    while name <> "" do
        match read() with
        | null -> yield (name, dna.ToString()); name <- ""
        | s when s.[0] = '>' ->
            if name <> ">" then yield (name, dna.ToString())
            name <- s.[1 ..]
            dna.Clear()
        | s -> dna.Append(s)
}
let printfnSeq fmt s =
    let mutable head = true
    for i in s do
        if not head then printf " "
        head <- false
        printf fmt i
    printfn ""
let a = fasta.ToArray()
let s = seq {
    for i = 0 to (snd a.[0]).Length - 1 do
        let d = a.Select(fun (name, dna) -> dna.[i]) |> Seq.countBy id |> dict
        let s = "ACGT".Select(fun k -> snd(d.TryGetValue(k))).ToArray()
        let sMax = s.Max()
        yield "ACGT".[Seq.findIndex (fun i -> i = sMax) s], s
}
new string(s.Select(fst).ToArray()) |> printfn "%s"
for i = 0 to 3 do
    printf "%c: " "ACGT".[i]
    s.Select(fun (_, stat) -> stat.[i]) |> printfnSeq "%d"
