#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let fasta = seq {
    let mutable name = ">"
    let mutable dna = System.Text.StringBuilder();
    while name <> "" do
        match read() with
        | null -> yield (name, dna.ToString()); name <- ""
        | s when s.[0] = '>' ->
            if name <> ">" then yield (name, dna.ToString())
            name <- s.[1 ..]
            dna.Clear()
        | s -> dna.Append(s)
}
let fa = fasta.ToArray() in Enumerable.Join(fa, fa,
    (fun (_, i) -> i.[(i.Length - 3) ..]), (fun (_, i) -> i.[.. 2]),
    (fun (n1, _) (n2, _) -> if obj.ReferenceEquals(n1, n2) then () else
        printfn "%s %s" n1 n2)) |> Seq.iter ignore
