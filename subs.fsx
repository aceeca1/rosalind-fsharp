#!/usr/bin/fsharpi
let read = System.Console.ReadLine
let printfnSeq fmt s =
    let mutable head = true
    for i in s do
        if not head then printf " "
        head <- false
        printf fmt i
    printfn ""
let s1, s2 = read(), read()
seq {
    let mutable i = 0
    while i <> -1 do
        i <- s1.IndexOf(s2, i)
        if i <> -1 then
            i <- i + 1
            yield i
} |> printfnSeq "%d"
