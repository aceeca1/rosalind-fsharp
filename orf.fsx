#!/usr/bin/fsharpi
open System.Linq
let read = System.Console.ReadLine
let fasta1 (read: unit -> string) =
    read()
    let sb = System.Text.StringBuilder()
    let mutable s = read()
    while not (isNull s) do
        sb.Append(s)
        s <- read()
    sb.ToString()
let codon = [|
    "UUU", 'F'; "CUU", 'L'; "AUU", 'I'; "GUU", 'V'
    "UUC", 'F'; "CUC", 'L'; "AUC", 'I'; "GUC", 'V'
    "UUA", 'L'; "CUA", 'L'; "AUA", 'I'; "GUA", 'V'
    "UUG", 'L'; "CUG", 'L'; "AUG", 'M'; "GUG", 'V'
    "UCU", 'S'; "CCU", 'P'; "ACU", 'T'; "GCU", 'A'
    "UCC", 'S'; "CCC", 'P'; "ACC", 'T'; "GCC", 'A'
    "UCA", 'S'; "CCA", 'P'; "ACA", 'T'; "GCA", 'A'
    "UCG", 'S'; "CCG", 'P'; "ACG", 'T'; "GCG", 'A'
    "UAU", 'Y'; "CAU", 'H'; "AAU", 'N'; "GAU", 'D'
    "UAC", 'Y'; "CAC", 'H'; "AAC", 'N'; "GAC", 'D'
    "UAA", '$'; "CAA", 'Q'; "AAA", 'K'; "GAA", 'E'
    "UAG", '$'; "CAG", 'Q'; "AAG", 'K'; "GAG", 'E'
    "UGU", 'C'; "CGU", 'R'; "AGU", 'S'; "GGU", 'G'
    "UGC", 'C'; "CGC", 'R'; "AGC", 'S'; "GGC", 'G'
    "UGA", '$'; "CGA", 'R'; "AGA", 'R'; "GGA", 'G'
    "UGG", 'W'; "CGG", 'R'; "AGG", 'R'; "GGG", 'G'|]
let codonD = dict codon
let rna = (fasta1 read).Replace('T', 'U').ToCharArray()
let rna2 = rna.Reverse().Select(fun i ->
    match i with
    | 'A' -> 'U'
    | 'C' -> 'G'
    | 'G' -> 'C'
    | 'U' -> 'A').ToArray()
let re = System.Text.RegularExpressions.Regex("(?=(M[^$]*)\\$)")
let ans = seq {
    for i in [|rna; rna2|] do
        for j = 0 to 2 do
            let r = i.Skip(j) |> Seq.chunkBySize 3
            let r1 = r.Where(fun i -> i.Length = 3)
            let r2 = r1.Select(fun i -> codonD.[new string(i)]).ToArray()
            let matches = re.Matches(new string(r2))
            let matchesT = matches.Cast<System.Text.RegularExpressions.Match>()
            yield! matchesT.Select(fun i -> i.Groups.[1].Value)
}
for i in ans.Distinct() do printfn "%s" i
